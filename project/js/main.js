/* Теоретичні питання.


1. Document Object Model (DOM) - програмний інтерфейс, що дозволяє 
працювати з усіма елементами HTML як з об'єктами: надавати їм властивості, 
отримувати та змінювати значення цих властивостей, видаляти тощо. 
Те, що для HTML було лише тегом, завдяки JS DOM стає об'єктом, 
та набуває його характеристик, що робить web-сторінку "живою". 


2. * Властивість innerText 
- дозволяє додати/отримати до HTML елемента текстовий контент 
(наприклад: "Привіт!" між тегами div). 
* Властивість innerHTML 
- додає/отримує HTML ябо XML розмітку дочірніх елементів, 
а також дозволяє розмістити в HTML елементі інший елемент з текстовим контентом 
та змінювати його.
(наприклад: тег з контентом <div>"Привіт!"</div> ), в т.ч. зберігає пробіли.


3. Звернутися до елемента сторінки можна кількома способами: 
* document.getElementById(id)
* getElementsByTagName(tag)
* getElementsByClassName(className)
* document.getElementsByName(name)
 
* Найбільш поширеним є querySelector() (querySelectorAll() 
для виведення всіх елементів за вказаним селектором). 
Він найбільш універсальний - можна знайти елемент за будь-яким його селектором.
------------------------------------------------------- */

// Практичні завдання.

/* ----------------------------------------------------- 
Знайти всі параграфи на сторінці (a) та встановити колір фону #ff0000 (b)
*/

// let paragraphs = document.querySelectorAll("p");

// for (let elem of paragraphs) {
//    elem.style.backgroundColor = '#ff0000';   
// }

/* ----------------------------------------------------
* Знайти елемент із id=”optionsList”. Вивести у консоль. 
* Знайти батьківський елемент та вивести в консоль. 
* Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
*/

// const elements = document.getElementById('optionsList'); 
// console.log(elements);

// const parents = elements.parentNode;
// console.log(parents);

// const childNodes = parents.childNodes;

// for (let i of childNodes) {
//     console.log(`Name: ${i.nodeName} Type:${i.nodeType}`)
// }

/* --------------------------------------------------------- 
Встановіть в якості контента елемента з класом testParagraph 
наступний параграф – This is a paragraph
*/

const testParagraph = document.getElementById('testParagraph');
console.log(testParagraph);

// testParagraph.textContent = 'This is a paragraph';
testParagraph.innerText = 'This is a paragraph';
console.log(testParagraph);


/* ------------------------------------------------------
 Отримати елементи, вкладені в елемент із класом main-header 
і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
*/

// const mainHeader = document.querySelector('.main-header');
// // console.log(mainHeader);
// const mainHeaderElements = mainHeader.children;

// for (let node of mainHeaderElements) {
//     console.log(node);
//     node.classList.add('nav-item')
// }

/* -------------------------------------------------------
Знайти всі елементи із класом section-title. 
Видалити цей клас у цих елементів.
*/

// let sectionTitle = document.querySelectorAll('.section-title');
// console.log(sectionTitle);

// for (let iterator of sectionTitle) {
//     iterator.classList.remove('section-title');
// }

// sectionTitle.forEach(element => {
//     element.classList.remove('section-title');
// });